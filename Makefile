#!/bin/bash
OS := $(shell uname)
timestamp := $(shell date +%Y-%m-%d_%H%M%S)
BUILD_FOLDER := "."

ifneq ($(shell test -f ./data/acme.json), 1)
    $(shell touch data/acme.json)
endif

ifneq (,$(wildcard ./.env))
    include .env
    export
else
    $(error Copy .env.sample to .env and fill all values first)
endif

ifeq ($(OS),Darwin)
        UID = $(shell id -u)
        GID = $(shell id -g)
else ifeq ($(OS),Linux)
        UID = $(shell id -u)
        GID = $(shell id -g)
else
        UID = 1000
        GID = 1000
endif

docker-compose := UID=${UID} GID=${GID} docker compose -p ${PROJECT_NAME} -f ${BUILD_FOLDER}/docker-compose.yml
docker := UID=${UID} GID=${GID} C_ENV=${CONTAINER_ENV} sudo docker

help: ## Show this help message
	@echo 'usage: make [target]'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'

upd: ## Run container daemonized
	sudo docker network inspect reverse-proxy >/dev/null 2>&1 || \
	sudo docker network create --driver bridge reverse-proxy
	${docker-compose} up -d --remove-orphans

down: ## Stop Container
	${docker-compose} down

reload: ## Stop and start container daemonized
	$(MAKE) down
	$(MAKE) upd

pull: ## Download last docker image
	${docker-compose} pull

update: ## Update container with lastest version
	$(MAKE) pull
	$(MAKE) reload

rm: ## Remove container
	${docker-compose} rm

## Shell commands
sh: ## Shell into container
	${docker-compose} exec traefik sh
shell: # Shell into container
	$(MAKE) sh
## Editor commands
edit: ## Edit docker-compose file
	editor ${BUILD_FOLDER}/docker-compose.yml
make: ## Edit Makefile
	editor Makefile
## Informative commands
config: ## Show docker config
	${docker-compose} config
log: ## Show logs inline help
	$(info Use either dlog, tlog or alog)
dlog: ## Show DOCKER's logs
	${docker-compose} logs
tlog: ## Show TRAEFIK's logs
	cat logs/traefik.log | jq
alog: ## Show ACCESS's logs
	cat logs/access.log | jq
clean-logs: ## Clean all logs
	rm logs/*.log
