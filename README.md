# Traefik Reverse Proxy with Docker

This project allows you to quickly setup a secure Traefik reverse proxy for other docker based projects

## Installation

1. Install docker with docker-compose, GNU Make and apache2-utils.
2. Clone this repo in your server (for example, in /srv)
3. Copy .env.sample into .env and change the values
```
cp .env.sample .env
```

4. Use apache2-utils's `htpasswd` command to generate your Basic Auth passwords. Add a user to auth/usersfile with the following command:

```
htpasswd -c auth/usersfile <username>
```

## Usage

Once you have everything configured, execute the following command in this folder and your reverse proxy will be enabled

```
make upd
```

You can find some middlewares ready to use in the config/traefik.d folder.

### Enable Traefik on other projects

All you need now is to add a few more labels to a running docker-compose.yml file

```yaml
services:
  ...:
    networks:
      - reverse-proxy
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=reverse-proxy"
      - "traefik.http.routers.<UNIQUE_IDENTIFIER>.entrypoints=websecure"
      - "traefik.http.routers.<UNIQUE_IDENTIFIER>.rule=Host(`domain.com`, `www.domain.com`)"
      - "traefik.http.routers.<UNIQUE_IDENTIFIER>.middlewares=force-non-www-urls@file"

networks:
  reverse-proxy: # this is the network provided by Traefik Reverse Proxy with Docker project
    external:
      name: reverse-proxy

```
